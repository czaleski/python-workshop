# Add a "capacity" attribute to the Bus
# child class.
# Also add a "fare" method to Bus, which
# is based on the max_speed and capacity:
# fare = max_speed / capacity * 1.00

class Vehicle:
    def __init__(self, name, max_speed, mileage):
        self.name = name
        self.max_speed = max_speed
        self.mileage = mileage

class Bus(Vehicle):
    pass
