# Write a function to create a new dictionary 
# by extracting the mentioned keys from a given
# dictionary.

sample_dict = {
    "name": "Kelly",
    "age": 25,
    "salary": 8000,
    "city": "New york"}

# Keys to extract
keys = ["name", "salary"]

# Expected output:
# {'name': 'Kelly', 'salary': 8000}