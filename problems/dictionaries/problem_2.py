# Write a function to print the value of 
# key ‘history’ from the given nested dictionary

sampleDict = {
    "class": {
        "student": {
            "name": "Mike",
            "marks": {
                "physics": 70,
                "history": 80
            }
        }
    }
}