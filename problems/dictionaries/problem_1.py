# Below are two lists. Write a function 
# to convert them into a dictionary in
# a way that item from list1 is the key
# and item from list2 is the value. Return
# the new dictionary

keys = ['Ten', 'Twenty', 'Thirty']
values = [10, 20, 30]


# Expected output:
# {'Ten': 10, 'Twenty': 20, 'Thirty': 30}


