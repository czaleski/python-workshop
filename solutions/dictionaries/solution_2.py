
sampleDict = {
    "class": {
        "student": {
            "name": "Mike",
            "marks": {
                "physics": 70,
                "history": 80
            }
        }
    }
}

def get_history(dict):
    # walkthrough examples...
    # print(dict['class'])
    # print(dict['class']['student'])
    # print(dict['class']['student']['marks'])
    
    print(dict['class']['student']['marks']['history'])
    # or...
    print(dict.get('class').get('student').get('marks').get('history'))
    

result = get_history(sampleDict)
# The function already prints, so what
# happens if I print the result here? Why?
# print(result)