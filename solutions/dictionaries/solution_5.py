

def horizontal_bar_chart(sentence):
    char_dict = {}
    for char in sentence:
        if char >= "a" and char <= "z":
            if char not in char_dict:
                char_dict[char] = ""
            char_dict[char] += char
    new_list = []
    for char_string in char_dict.values():
        new_list.append(char_string)
    new_list.sort()
    return(new_list)

result = horizontal_bar_chart("abba has a banana")
for s in result:
    print(s)