
def divides_unevenly(price, quantity):
    if price == 0 or quantity == 0:
        return None
    quotient = price / quantity
    if int(quotient) == quotient:
        return False
    else:
        return True
    

result = divides_unevenly(6, 1.5)
print(result)
result = divides_unevenly(6, 4)
print(result)
result = divides_unevenly(0, 6)
print(result)